Merger / réintégrer une branche feature_name:
git merge --no-ff feature_name -m “commentaire”

▸ Gérer un conflit sur fichier.txt:
git add fichier.txt 
git commit-m “commentaire de merge”

▸ Abandonner un merge en cours:
git merge --abort