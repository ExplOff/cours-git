Déplacer la base de la branche ou rebaser la branche

git rebase [ref]

git rebase -i [ref]

Permet d'avoir un choix pour chaque merge :
- p, pick = use commit
- r, reword = use commit, but edit the commit message
- e, edit = use commit, but stop for amending
- s, squash = use commit, but meld into previous commit
- f, fixup = like "squash", but discard this commit's log message
- x, exec = run command (the rest of the line) using shell

git rebase --abort (annuler le rebase en cours idem pour merge)